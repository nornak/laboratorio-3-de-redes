# Laboratorio N°3 de redes


Crear un juego de blackjack. En donde se tiene que llegar a 21 entre las cartas
que se tiene. Las cartas numericas suman su valor:

+ las figuras suman 10 
+ y el As es un 11 o un 1, el jugador elige que le conviene

La banca reparte dos cartas a cada jugador, incluyo a ella

El jugador:
    + puede plantarse, es decir que se queda conforme con las cartas que tiene
    + puede pedir cartas, si se pasa de 21 pierde

La banca:
    + esta obligado a pedir cartas si tiene menos de 16 o 16
    + se planta cuando tiene 17 cartas

El ganador es aquel jugador que tiene el puntaje (suma de cartas) mas alto entre todos o el que
obtiene 21 (blackjack).

Debe tener sistema de apuestas, cada jugador apuesto lo que le plazca. El maximo
de las apuestas es de 10 fichas.
Cada jugador tiene 100 fichas

Una vez establecidas las apuestas:
    * Tomara dos cartas para si misma.
    * La banca pasa dos cartas a cada jugador.
    * Los jugadores decidiran si plantarse o tomar otra carta
Cuando todos los jugadores estan plantados:
    * Se calculan los resultados
    * se elige un ganador
    * y la casa paga o cobra lo correspondiente
El juego termina cuando
    * todos los jugadores se retiran
    * si solo se retirna uno, el otro sigue jugando contra la banca
    

Fases del programa

* Preparacion
    1. Se ejecuta el servidor
    2. Se ejecuta los jugadores
    3. los jugadores se conectan al servidor
    4. El servidor informa de la conexion

* Apuesta:
    1. el servidor pregunta el monto de las apuestas
    2. Los jugadores responden
    3. Se descuentan las fichas y se crea un boto de apuestas totales

* juego:
    8. el servidor genera un par de cartas
    9. Los jugadores reciben las cartas
    10. Los jugadores informan si se planta o pide otra carta
    11. El servidor entrega carta hasta que los jugadores dejen de pedir
    12. Cuando todos los jugadores se han planteadoo, o han  quedado eliminados,
        el sever calcula al ganador
    13. el server informa a los ganadores y perdedores
    14. se hace el pago o cobros de la apuesta, actualizar fichas

* Cierre:
    15. Al terminar la partida el jugador decide si se retira o sigue
    16. El server cierra la session del jugador retirado
        * si hay jugadores activos, seguir jugando
    17. Cuando ya no quedan jugadores, se cierra el programa

    
