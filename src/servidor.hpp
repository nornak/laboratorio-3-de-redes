#ifndef _H_SERVIDOR_
#define _H_SERVIDOR_

#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/un.h>
#include <netdb.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>

#include <iostream>
#include <sstream>
#include <vector>

#include "mazo.cpp"
#include "conexion.cpp"

using namespace std;

typedef struct dato_cliente{
    int id;
    vector<carta> *mazo;
    int descriptor;
    int *puntaje;
}dato_cliente;

typedef struct dato_banca{
    vector<carta> *mazo;
    // TODO -> todos los descriptores
    int *descriptores;
    // TODO -> arreglo con jugadores que ya apostaron
    // arreglo de puntajes
    int *puntajes;

    // el id para esperar la respuesta de este thread
    pthread_t *t_reloj_id;
    bool *lleno;
}dato_banca;


// TODO dar un numero a los clientes para poder identificarlos, el mismo que
// posee los thread hilos

int main(int argc, char **argv);


void enviarPrimeraMano(int *descriptor);

void enviarCarta(carta c, int descriptor);

string convertInt(int number);

#endif

