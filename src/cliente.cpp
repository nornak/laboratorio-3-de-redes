#include "cliente.hpp"

using namespace std;

int puerto= 255523;


int main(int argc,char **argv)
{

	//SE CONSULTA POR DIRECCION IP DEL SERVIDOR
	printf("\nIngrese direccion IP del servidor: ");
	char ip[15];
	cin >> ip;
	

	//*********** SE DEFINE LA ESTRUCTURA DEL CLIENTE****************
	int Descriptor = iniciarCliente(puerto, ip);
    int conexion = 0;
    int fichas = 8;
    bool seguir = true;
    
    conexion = recibirMensaje(Descriptor); 

    //CLIENTE
    while(fichas > 0 and seguir and conexion == 1){
         
        int numero = 0;
        vector <carta> cartaMano;

        numero = preguntarFicha(fichas);

        //ENVIO DE FICHA AL SERVIDOR
        if(send( Descriptor,(char *) &numero, sizeof(numero), 0) == -1)
        {
            cout << "Error al enviar datos a servidor" << endl;
            cout << endl;

        }else{
            cout << "Se envío Correctamente las fichas de apuesta" << endl;
            cout << endl; 
            fichas = fichas - numero;
        }

        //RECIBIR CARTAS
        carta aux1;
        carta aux2;

        aux1 = recibirCarta(Descriptor);
        cartaMano.push_back(aux1);
        aux2 = recibirCarta(Descriptor);
        cartaMano.push_back(aux2);   
        
        //DECIDE SI PLANTARSE O NO
        numero = Decision(1); 
        cout << "enviando" << endl; 
        
        if(send( Descriptor, (char*) &numero, sizeof(numero), 0) == -1)
        {
            cout << "Error al enviar datos a servidor" << endl;
        }else{
    
            cout << endl;
            
            //DECIDE SI QUIERE SEGUIR 
            if(fichas > 0){
                numero = Decision(2);

                if(numero == 2){
                    seguir = false;
                }
     

            }else{
                cout << "No te queda fichas no puedes seguir" << endl;
                seguir = false;           
            }
        }
    }

    if(conexion != 1)
    {
        cout << "Error de conexion" << endl;
    }

	close(Descriptor);

}








int Decision(int tipo){
    
    char opcion[20];
    string pregunta;
    int numero = 0;
    

    //SELECCION DE PREGUNTA
    if(tipo == 1){
        pregunta = "Deseas Plantarse (1) o sacar más carta (2)?: ";
    }else{
        pregunta = "Deseas Continuar (1) o Retirarse (2) ?: ";
    }

    //PREGUNTAR OPCIONES
    memset(opcion, 0, 20);
    cout << pregunta;
    cin >> opcion;

    if(sscanf(opcion, "%d", &numero) <= 0)
    {
            cout << "Error la opción ingresada no es un número" << endl;
    }

    cout << endl;

    //VALIDACIÓN DE OPCIONES
    while(numero != 1 and numero != 2){
        cout << "Opcion ingresada no es válida." << endl;
        cout << pregunta;
        cin >> opcion;  
        
        if(sscanf(opcion, "%d", &numero) <= 0)
        {
            cout << "Error la opcion ingresada no es un número" << endl;
        }

        cout << endl;
    }

    return numero;

}

int preguntarFicha(int fichasTotales){
    
    //INGRESAR CANTIDAD DE APUESTA
    char cantidadFicha[20];
    int numero = 20;
    int resto = 0;

    memset(cantidadFicha, 0, 20);
    
    cout << "Ingrese cantidad de ficha que desea apostar: ";
    cin >> cantidadFicha;
    
    if(sscanf(cantidadFicha, "%d", &numero) <= 0)
    {
            cout << "Error el dato ingresado no es un número" << endl;
    }else{
        resto = fichasTotales - numero;

        if(resto < 0)
        {
            cout << "No tienes esa cantidad de fichas" << endl;
        }
    }

    cout << endl;

    //VALIDACION DE LA APUESTA
    while( numero > 10 or resto < 0){
        cout << "Recuerde que la máxima apuesta es de 10 fichas." << endl;
        cout << "Ingrese Nuevamente: ";
        cin >> cantidadFicha; 
        
        if(sscanf(cantidadFicha, "%d", &numero) <= 0)
        {
            cout << "Error el dato ingresado no es un número" << endl;
        }else{
            resto = fichasTotales - numero;
            
            if(resto < 0)
            {
                cout << "No tienes esa cantidad de fichas" << endl;
            }
        }
        cout << endl;
    }

    return numero;

}
void mostrarMano(vector<carta> v)
{
    for (int i = 0; i < v.size(); i++) 
    {
        cout << v[i].numero << " de " << v[i].figura << endl;
    }
}

carta casteoMsj(char *msj)
{
    carta c;
    return c;
}

carta recibirCarta(int &Descriptor){
    
    char respuesta[25];
    char nombre[25];
    int numero = 0;
    carta c;

    memset(respuesta, 0, 25); 
    memset(nombre, 0, 25);

    if(recv(Descriptor, respuesta, sizeof(respuesta), 0) == -1){
        cout << "Error al recibir carta del servidor" << endl;
    }
    
    if(sscanf(respuesta, "%s %d", nombre, &numero) <= 0){
        cout << "Carta con error" << endl;
    } 

    c.figura = new string(nombre);
    c.numero = numero;

    return c; 
}
