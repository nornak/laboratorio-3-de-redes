#include "mazo.hpp"

vector<carta> crearMazo(){
    

    vector<carta> mazo(52);
    int j = 1;

    for(int i = 0; i < 13; i++){
        
        mazo[i].figura = new string("pica");
        mazo[i].numero = j;
        j++;

    }

    j = 1;

    for(int i = 13; i < 26; i++){
        
        mazo[i].figura = new string("corazones");
        mazo[i].numero = j;
        j++;

    }

    j = 1;

    for(int i = 26; i < 39; i++){
        
        mazo[i].figura = new string("Diamantes");
        mazo[i].numero = j;
        j++;

    }
    
    j = 1;
    
    for(int i = 39; i < 52; i++){
        
        mazo[i].figura = new string("Trebol");
        mazo[i].numero = j;
        j++;

    }

    return mazo;
}

carta sacarCarta(vector<carta> &mazo){
    
    srand(time(NULL));
    int n = 0;
    int x = mazo.size();
    
    n = rand()%x;

    carta auxiliar;
    auxiliar.figura = new string(*mazo[n].figura);
    auxiliar.numero = mazo[n].numero;

    mazo.erase(mazo.begin() + n);

    return auxiliar;
}

