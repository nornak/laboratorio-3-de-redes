#ifndef _H_CLIENTE_
#define _H_CLIENTE_

#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/un.h>
#include <netdb.h>
#include <string.h>
#include <signal.h>

#include <iostream>
#include <stack>
#include <list>
#include <ctime>

#include "mazo.cpp"
#include "conexion.cpp"

using namespace std;

int Decision(int tipo);
int preguntarFicha(int fichasTotales);
int main(int argc, char **argv);
carta recibirCarta(int &Descriptor);
    
void mostrarMano(vector<carta> v);

carta casteoMsj(char *msj);

#endif
