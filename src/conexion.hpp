#ifndef _CONEXION__H_
#define _CONEXION__H_

#include <string>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <iostream>


using namespace std;

int iniciarServidor(int puerto);

int iniciarCliente(int puerto, char *ip);


int abrirSocket(int &descriptor);

int bindSocket(int &descriptor, int puerto);

int bindSocket(int &descriptor, int puerto, char *ip);

int conectar(int &descriptor, int puerto, char  *ip);

int empezarEscucha(int &descriptor, int n);

int aceptarConexion();

int enviarMensaje();

int recibirMensaje(int &Descriptor);


#endif
