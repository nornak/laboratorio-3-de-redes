#include "servidor.hpp"

const int puerto = 255523;
const int max_cliente = 7;
int num_cliente = 0;
vector<carta> m;

int main(int argc,char **argv)
{
    cout << "Servidor" << endl;
    int apuestas = 0;
    int puntos[2];

    m = crearMazo();

    int descriptor = iniciarServidor(puerto);

    listen(descriptor, max_cliente);

    struct sockaddr_in client[2]; 
    int descriptor_entrante[2];
    int i = 0;

    while( i < 2)
    {
        if((descriptor_entrante[i] = aceptarConexion(descriptor, client[i])) == -1 )
            cout << "asd" << endl;
        else{
            cout << "conexion establecida" << endl;
        }
        i++;
    }
    int respuesta = 1;

    send(descriptor_entrante[0],(char *) &respuesta , sizeof(int), 0);
    send(descriptor_entrante[1],(char *) &respuesta , sizeof(int), 0);

    enviarPrimeraMano(descriptor_entrante);

    recv(descriptor_entrante[0], (char *)&respuesta, sizeof(int), 0);
    cout << "respuesta recibida de c1 " << respuesta << endl;
    apuestas += respuesta;

    recv(descriptor_entrante[1], (char *)&respuesta, sizeof(int), 0);
    cout << "respuesta recibida de c2 " << respuesta << endl;
    apuestas += respuesta;
    
    return 0;
}


void enviarPrimeraMano(int *descriptor)
{
    int i = 0;

    carta c = sacarCarta(m);
    enviarCarta(c , descriptor[i]);
    c = sacarCarta(m);
    enviarCarta(c , descriptor[i]);
}



void enviarCarta(carta c, int descriptor)
{

    string entrega = c.figura->c_str();
    entrega += " " + convertInt(c.numero);
    send(descriptor, entrega.c_str(), sizeof(entrega.c_str()), 0);
}

string convertInt(int number)
{
   stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}
