
#include "conexion.hpp"



int iniciarServidor(int puerto)
{
    cout << "iniciando servidor" << endl;

    int descriptor;

    if(abrirSocket(descriptor) == -1)
    {
        cout << "error al abrir el descriptor" << endl;
        return -1;

    }

    if(bindSocket(descriptor, puerto) == -1)
    {
        cout << "error al " << endl;
        return -1;
    }

    empezarEscucha(descriptor, 1);

    return descriptor;
}

int iniciarCliente(int puerto, char *ip)
{
    int descriptor;

    if(abrirSocket(descriptor) == -1)
    {
        cout << "Error al crear el descriptor" << endl;
        return -1;
    }

    if(conectar(descriptor, puerto, ip) == -1)
    {
        cout << "Error para conectarse con el servidor" << endl;
        return -1;
    }

    return descriptor;
}

// abre el socket y retorna el descriptor del socket
int abrirSocket(int &descriptor)
{
	descriptor = socket(AF_INET, SOCK_STREAM, 0);

    return descriptor;
}

int bindSocket(int &descriptor, int puerto)
{
    struct sockaddr_in direccion;

    direccion.sin_family = AF_INET;
    direccion.sin_port = htons(puerto);
    direccion.sin_addr.s_addr = INADDR_ANY;

    return bind (descriptor, (struct sockaddr *) & direccion, sizeof(direccion));
}


int bindSocket(int &descriptor, int puerto, char *ip)
{
    struct sockaddr_in direccion;

    direccion.sin_family = AF_INET;
    direccion.sin_port = htons(puerto);
    direccion.sin_addr.s_addr = inet_addr(ip);

    return bind (descriptor, (struct sockaddr *) & direccion, sizeof(direccion));
}

int conectar(int &descriptor, int puerto, char *ip)
{

    struct sockaddr_in server;  

    server.sin_family = AF_INET;

    server.sin_port = htons(puerto); 

	server.sin_addr.s_addr = inet_addr(ip);

    bzero(&(server.sin_zero),8);

    return connect(descriptor, (struct sockaddr *)&server, sizeof(struct sockaddr));
}

int empezarEscucha(int &descriptor, int n)
{
    listen(descriptor, n);
    return 0;
}

int recibirMensaje(int &Descriptor){

    int buffer;

    if(recv(Descriptor, (char*) &buffer, sizeof(buffer), 0) == -1)
    {
        printf ("Error al leer los datos del servidor\n");
    }

    if(buffer == 1){
        cout << "Se conectó correctamente con el servidor" << endl;
    }else{
        buffer = 0;
    }    

    return buffer;
}

int aceptarConexion(int descriptor, sockaddr_in &cliente)
{
    socklen_t sin_size = sizeof(struct sockaddr_in);

    return accept(descriptor, (struct sockaddr *)&cliente, &sin_size);
}


