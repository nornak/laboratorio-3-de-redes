
#Compilacion de los archivos

CFLAG = -Wall -g -Wshadow -Wpointer-arith -Wcast-qual -lpthread 

build: cliente servidor

cliente:  build/cliente.o build/mazo.o
	g++  $(CFLAG) $< -o $@

servidor:  build/servidor.o build/conexion.o build/mazo.o 
	g++  $(CFLAG) $< -o $@

build/cliente.o: src/cliente.cpp src/cliente.hpp 
	g++ -c $(CFLAG) $< -o $@
	
build/servidor.o: src/servidor.cpp src/servidor.hpp 
	g++ -c $(CFLAG) $< -o $@

build/conexion.o: src/conexion.cpp src/conexion.hpp 
	g++ -c $(CFLAG) $< -o $@

build/mazo.o: src/mazo.cpp src/mazo.hpp 
	g++ -c $(CFLAG) $< -o $@

clean:
	rm build/*



                              
